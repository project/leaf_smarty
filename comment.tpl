<div class="commentbody {if ($comment->new)}comment-new{else}{/if}"> 
  {if $comment->new} 
  <a id="new"></a> <span class="new">{$new}</span> 
  {/if} 
  <h4 class="c_userlink">{$author} wrote:</h4> 
  <div class="c_commentbody">{$content}</div> 
  {if $picture} 
  <br class="clear" /> 
  {/if} 
  <div class="c_commentinfo">{$date}</div>
  <p class="postmetadata">{$links} &#187;</p> 
</div>