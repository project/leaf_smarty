<div class="contentbody{if ($sticky)} sticky{else}{/if}"> 
  {if $page == 0} 
  <h3><a href="{$node_url}" rel="bookmark" title="Read Entry: {$title}">{$title}</a></h3> 
  {/if} 
  {$content}
  {if $links} 
  <div class="itemdetails">
  <span class="item2">{$terms}</span>
  <span class="item3">{$links}</span>
  </div> 
  {/if} 
</div>