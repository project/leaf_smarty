<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{$language}" xml:lang="{$language}">
<head>
<title>{$head_title}</title>
<meta http-equiv="Content-Style-Type" content="text/css" />
{$head}{$styles}
</head>

<body id="home">
	<div id="header">
		<h1><a href="{url}" title="{$site_name}">{$site_name}</a></h1> 
		<div class="slogan">{$site_slogan}</div> 
			<div id="navigation"><h3 class="hidden">Navigation</h3>
			<ul> 
			<li>{$primary_links}</li>
			</ul></div>
	</div>


<div id="wrapper">
	<div id="container">
		<div id="content">
			<div class="contentdiv">
				<h2>{$site_name}</h2>
				<div class="divweblog">
				<!--Database Generated Content-->
					<div class="navigation"> {$breadcrumb} </div> 
					{if $messages != ""} 
					<div id="message">{$messages}</div> 
					{/if} 
					{if $mission != ""} 
					<div id="mission">{$mission}</div> 
					{/if} 
					{if $title != ""} 
					<h2 class="page-title">{$title}</h2> 
					{/if} 
					{if $tabs != ""} 
					{$tabs} 
					{/if} 
					{if $help != ""} 
					<p id="help">{$help}</p> 
					{/if} 
					<!-- start main content --> 
					{$content} 
					<!-- end main content --> 
				<!--End Database Generated Content-->
	      	</div>
    		</div>
  		</div>
	</div>

<div id="sidebar">
	{if $search_box} 
		<form action="{$search_url}" method="post" id="searchform"> 
		<input class="form-text" type="text" size="15" value="" name="edit[keys]" id="s" /> 
		<input class="form-submit" type="submit" value="{$search_button_text}" id="searchsubmit" /> 
		</form> 
	{/if} 
	{$sidebar_left}
	{$sidebar_right}
</div>


<div class="clearing">&nbsp;</div>

<!--Close Wrapper-->
</div>

<div id="footer">
	<div id="credits">Design by <a href="http://www.stanch.net">Stanch.net</a> | <a href="http://validator.w3.org/check?uri=referer">XHTML 1.0 Strict</a> | <a href="http://jigsaw.w3.org/css-validator/">CSS</a> | <a href="http://skins.nucleuscms.org/xml-rss2.php">Hungry? Feed on RSS</a><br />Powered by <a href="http://www.drupal.org">Drupal</a> | <a href="http://www.spreadfirefox.com/?q=affiliates&amp;id=58615&amp;t=73">Get a better browser</a></div>
</div>
{$closure} 
</body>
</html>
